﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace Assets.tests
{
    public class SteeringTests
    {
        [Test]
        public void Left_Arrow_Turns_Left()
        {
            Assert.AreEqual(1,1);
        }
    }
}
